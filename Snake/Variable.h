#ifndef VARIABLE_H_H_H
#define VARIABLE_H_H_H

#define TIMER_1				1											//定时器ID
#define SQUARE_SIZE			30											//方块像素长宽大小
#define WORK_WINDOW_HEIGHT	20											//工作窗口高度方向小方块个数
#define WORK_WINDOW_WIDTH	20											//工作窗口宽度方向小方块个数
#define WORK_WINDOW_AERA	WORK_WINDOW_HEIGHT*WORK_WINDOW_WIDTH		//工作窗口总的小方块个数

typedef enum tagDIRECTION {UP,DOWN,LEFT,RIGHT} DIRECTION;//自定义运动方向数据类型

extern BOOL			bIsGameStart;										//游戏开始与暂停标记
extern DIRECTION	dirNextMove;										//蛇头下一次运动方向
extern POINT		ptApplePos;											//当前苹果位置
extern POINT		ptSnakePos[WORK_WINDOW_AERA];	//当前蛇身的一系列位置集合，从蛇尾向蛇头存储以便于插入新蛇头
extern UINT			uiSnakeLength;										//蛇身长度
extern UINT			uiTimerElapse;										//定时器间隔时间

#endif