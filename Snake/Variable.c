#include <Windows.h>
#include "Variable.h"

BOOL		bIsGameStart;										//游戏开始与暂停标记
DIRECTION	dirNextMove;										//蛇头下一次运动方向
POINT		ptApplePos;											//当前苹果位置
POINT		ptSnakePos[WORK_WINDOW_AERA];						//当前蛇身的一系列位置集合，从蛇尾向蛇头存储以便于插入新蛇头
UINT		uiSnakeLength;										//蛇身长度
UINT		uiTimerElapse;										//定时器间隔时间
