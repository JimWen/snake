#include <Windows.h>
#include "DrawAndShow.h"
#include "Variable.h"

/************************************************************************/
/*功能：绘制工作窗口网格
**@20130527 by 文洲*/
/************************************************************************/
void DrawWorkGrid(HDC hdc)
{
	int i;

	//绘制水平线
	for (i=0; i<=WORK_WINDOW_HEIGHT; i++)
	{
		MoveToEx(hdc, 0, i*SQUARE_SIZE-i, NULL);
		LineTo(hdc, WORK_WINDOW_WIDTH*SQUARE_SIZE-WORK_WINDOW_WIDTH, i*SQUARE_SIZE-i);
	}

	//绘制竖直线
	for (i=0; i<=WORK_WINDOW_WIDTH; i++)
	{
		MoveToEx(hdc, i*SQUARE_SIZE-i, 0, NULL);
		LineTo(hdc, i*SQUARE_SIZE-i, WORK_WINDOW_HEIGHT*SQUARE_SIZE-WORK_WINDOW_HEIGHT);
	}
}

/************************************************************************/
/*功能：绘制一个小方格
**@20130527 by 文洲*/
/************************************************************************/
void DrawSquare(HDC hdc, const POINT pt)
{
	Rectangle(hdc, 
			  pt.x*SQUARE_SIZE-pt.x, 
			  pt.y*SQUARE_SIZE-pt.y,
			  pt.x*SQUARE_SIZE-pt.x+SQUARE_SIZE,
			  pt.y*SQUARE_SIZE-pt.y+SQUARE_SIZE);
}

/************************************************************************/
/*功能：绘制蛇身
**@20130527 by 文洲*/
/************************************************************************/
void DrawSnake(HDC hdc, const BOOL bIsClearSnake)
{
	int i;

	SaveDC(hdc);

	//根据参数确定是擦除旧蛇身还是绘制新的蛇身来选择画刷
	if (TRUE == bIsClearSnake)
	{
		SelectObject(hdc, (HBRUSH)GetStockObject(WHITE_BRUSH));
	} 
	else
	{
		SelectObject(hdc, (HBRUSH)GetStockObject(GRAY_BRUSH));
	}

	//从蛇尾向蛇头绘制
	for (i=0; i<uiSnakeLength; i++)
	{
		DrawSquare(hdc, ptSnakePos[i]);
	}

	RestoreDC(hdc, -1);
}

/************************************************************************/
/*功能：绘制苹果
**@20130527 by 文洲*/
/************************************************************************/
void DrawApple(HDC hdc)
{
	SaveDC(hdc);

	SelectObject(hdc, (HBRUSH)GetStockObject(GRAY_BRUSH));
	DrawSquare(hdc, ptApplePos);

	RestoreDC(hdc, -1);
}